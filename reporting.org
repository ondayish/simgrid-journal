
* Development
* Journal
** Week 1, [May 14]
- Got a gitlab account (inria)
- Installed spacemacs (I'm lovin' it, having second thoughts about "vim")
- Handled some settling in stuff: bus pass, room WiFi, etc.
*** Blocking points and questions:
- SimGrid design
- When can I expect to get a desktop PC?
*** TODO Summarize the 2 papers 
**** DONE Read "Verifying MPI applications with SimGridMC"
**** TODO Read the other paper
**** TODO Write a summary of each paper

** Week 2, [May 21]
*** DONE Meet up with Professor Quinson
    CLOSED: [2018-05-22 Tue 19:28]
    - CLOSING NOTE [2018-05-22 Tue 19:28] \\
      We talked about the architecture of simgrid.
**** Questions:
- Is ckpting in SimGrid done while in user-space or while in kernel-space?
  To my understanding it is done while in user-space.
 
- DPOR. Execution paths in SimGRID are constructed as the simulated 
  application runs. How can we run DPOR when we don't know the paths apriori?

- In [["https://hal.inria.fr/hal-01632421/document"][Verifying MPI Applications with SimGridMC]] They did mention "memory compression". Is the hashed memory 
  compressed?
  Would we need to uncompress it when we check if 2 states are equal?
**** After the meeting:
***** DONE Read [[http://simgrid.gforge.inria.fr/tutorials/simgrid-mc-101.pdf][SimGrid MC 101]] 
      CLOSED: [2018-06-07 Thu 10:29]
      - CLOSING NOTE [2018-06-07 Thu 10:29] \\
        "receive()" as an indecision point:
        When a process calls "receive()" it has no control over the order of messages it receives.
        Process 1 might have sent its message before process 2 or vice-versa.
        
        After understanding that fact things became clearer. Nonetheless, I'm not sure I fully 
        understand how SIMIX works, which is important for someone to understand anything about 
        simgrid, I think.
***** TODO Inside mc directory, put files into appropriate directories
- Now there is only "checker" and "remote" inside mc
  Put the scattered files into appropriate folders
  All this is to make the code more readable.
  - ckpt
  - state comparison
  - etc.
  Each should have its own directory, it makes the code more readable

*** TODO Delve into the code of SimGrid
**** TODO Run MPI example tests
**** TODO Understand what the code in sr/mc does
*** DONE Familiarize myself with IRC (tomorrow)
    CLOSED: [2018-05-23 Wed 11:29]
    - CLOSING NOTE [2018-05-23 Wed 11:29] \\
      Installed "hexChat"
      Added "irc.debian.org" to the list of servers
      Joined #simgrid channel
*** DONE Read [[http://dwarfstd.org/doc/Debugging%20using%20DWARF-2012.pdf][DWARF format]] 
    CLOSED: [2018-05-24 Thu 18:35]
    - CLOSING NOTE [2018-05-24 Thu 18:35] \\
      simgrid leverages Dwarf to handle alignment in structures.
      I'm still oblivious of how it does it.
      This is will be my first quest:) my next TODO

*** TODO Find out how simgrid leverages Dwarf
- Will do so by reading the code in simgrid/sr/mc/
- This should be a self contained directory

** Week 3, [May 28 - June 3]
*** DONE Review the workflow of /src/mc/checker
CLOSED: [2018-06-11 Mon 10:55]
- CLOSING NOTE [2018-06-11 Mon 10:55] \\
  It's not clear which part handles:
   - Dwarf 
   - stack unwinding
   - ckpting
   - etc.
*** TODO Find out which code base handles checkpointing
*** "Out of Tree Compilation" did not compile the examples in examples/msg/mc
**** TODO report this the SimGrid team:)

** Week 4, [June 4 - June 10]
*** DONE Read about Boost Unit Test Framework
CLOSED: [2018-06-11 Mon 10:54]
- CLOSING NOTE [2018-06-11 Mon 10:54] \\
  Just read the beginners tutorial.
*** TODO Implement the "hash" function
*** TODO Test my implementation

** Week 5, [June 11 - June 17]
*** DONE Meeting with prof. Quinson
    CLOSED: [2018-06-14 Thu 10:05]
    - CLOSING NOTE [2018-06-14 Thu 10:05]
-checker
-remote
-state eq:
 * dwarf 
 * hashing
-Snapshot dir (my thing): refactor this part
 * reusable by both simgrid and dmtcp
 * maybe this API: 
          - id = ckpt(pid, regionMasks)
          - restore(pid, id)
          
          - sosp::ProcDescriptor pid_maps = sosp::Attach(int pid) to get the mem regions to ckpt
          - sosp::Snapshot snap = sosp::ckpt(pid_maps)
            sosp::Snapshot snap = sosp::ckpt(pid_maps, maps.filter)
          - sosp::restore()/restore(pid_maps)
          
 * dependency: (useful)
   - depends on SimGrid
   - conditional depends on DMCTP
   - Simgrid does not depend on DMCTP
   - But the same SOSP should work for both DMTCP and simgrid
 * Plan:
   - Write unit tests for the existing code
     (mc_snapshot/PageStore.cpp would serve as an example
      - convert them to Boost.Test tests
      - TDD is the way to go if you're implementing an API
   - check simgrid jenkins
   - check out "Under the hood": coding syntax
   - Integration test:
     * pedagogical test: examples
     * pathological test: testsuits (tesh: test shell)
     
*** Useful links:
- [[https://github.com/randomstuff/simgrid-journal/blob/master/journal.org][Gabriel's journal on simgrid (2015-2016)]]
- [[https://tel.archives-ouvertes.fr/tel-01751786v2/document][Marion's thesis]]
Good references to understand the nitty-gritty details of "memory inspection."

*** TODO Refactor the code base that handles ckpting 
*** DONE Meeting with prof. Quinson
    CLOSED: [2018-06-14 Thu 11:03]
    - CLOSING NOTE [2018-06-14 Thu 11:03] \\
      - #nces between dmtcp and simgrid
       * simgrid does rewind
       * dmtcp we m
       * algo:
          - pages [][][]
          - snapshots: each snapshot has links to pages
          - [research idea: ]
          - snapshot/libsosp/CMakeList.txt
            * Mostly refactor
            * change if necessary
          - TODO:
            * identify the files
            * fork simgrid
            * pull request 1: convert 1 test to boost
              pull request 2: convert the other test 
              pull request 3: extent the tests for better coverage of the API (snapshot)
              pull request 4: move the files into snapshot dir and make sure simgrid still coompiles
              pull request 5: improve OOP/API if need
                              make snapshot dir independent
                              Allow to write to disk [the research idea]
                              change DMTCP to use it

- the new way: tools/cmake/Tests.cmake:
  Boost is there
  2nd and 3rd lines are common
- the old way: tools/cmake/unit...
- 2 pull requests 

*** DONE Identify the files handling ckpting
    CLOSED: [2018-06-14 Thu 16:57]
    - CLOSING NOTE [2018-06-14 Thu 16:57]

     The candidate files:
       - AddressSpace.hpp (the base class for both Process and Snapshot subclasses)
       - ChunkedData.hpp/cpp
       - compare.cpp
       - mc_checkpoint.cpp
       - mc_ignore.hpp
       - mc_mmu.hpp
       - mc_page_snapshot.cpp
       - mc_snapshot.hpp/cpp
       - PageStore.hpp/cpp
       - RegionSnapshot.hpp/cpp 

** Week 6 [June 18 - June 24]
*** TODO Add BOOST unit tests:
- mc_checkpoint.cpp:
  * add_region()
  * add_memory_regions()
- MC_load_dwarf() seems to be failing on BOOST.Test framework. Should dig into why it's behaving as such.
- Improve the API:

** Week 6 [June 25 - July 1]
- Did my first pull request to SimGrid to commit the 2 BOOST unit tests:) Yay!
- add the new test: tomorrow
- add put the files in the same snapshot dir

** Week 7 [July 2 - July 8]
- Refactoring the code base(checkpointing)
- Meeting with Martin:
 - boabab to view files and stuff
 - next meeting: wedn. at 9 am
*** Refactoring the code base(checkpointing)
**** PageStore.cpp/hpp
- sosp dependences:
  * src/mc/mc_mmu.hpp
- non-sosp dependences:
  * src/mc/mc_forward.hpp
  * xbt/base.h

- classes and functions:
  * stores pages of #nt snapshots
    a fixed size memory is preallocated to accommodate #nt snapshots
    (the size does grow dynamically)         
 
  * hashes pages to avoid saving page duplicates
    pages of the same hash value are put in the same hash set
    pages in the same hash set do not necessarily have the same content

**** mc_snapshot.cpp/hpp
- sosp dependences:
  * src/mc/RegionSnapshot.hpp
 
- non-sosp dependences:
  * src/mc/ModelChecker.hpp
  * src/mc/mc_unw.hpp
  * src/mc/remote/RemoteClient.hpp

- classes and functions:
  * snapshot:
    - constructor takes "RemoteClient*" process and state-number as input
    - take_snapshot()     // defined in mc_checkpoint.cpp
    - restore_snapshot()  // defined in mc_checkpoint.cpp

**** RegionSnapshot.cpp/hpp
- sosp dependences:
  * src/mc/ChunkedData.hpp
  * src/mc/PageStore.hpp
- non-sosp dependences:
  * xbt/base.h
  * src/mc/AddressSpace.hpp
  * src/mc/remote/RemotePtr.hpp

- classes and functions:
  * RegionSnapshot:
    A region can have one of the following storage type:
    - Flat/dense
    - Chunked: divided into pages to avoid having duplicates in the PageStore
    - Privatized: mpi?!

**** ChunkedData.cpp/hpp
- sosp dependences:
  * src/mc/PageStore.hpp
- non-sosp dependences:
  * src/mc/mc_forward.hpp

- classes and functions:
  * ChunkedData:
    - Has a pointer to the page store
    - Has page numbers of the chunks

**** mc_checkpoint.cpp
- sosp dependences:
src/mc/mc_mmu.hpp"
src/mc/mc_snapshot.hpp"
src/mc/RegionSnapshot.hpp"
 
- non-sosp dependences:
src/internal_config.h
src/mc/mc_private.hpp
src/smpi/include/private.hpp
xbt/file.hpp
xbt/mmalloc.h
xbt/module.h
src/simix/smx_private.hpp
src/mc/mc_private.hpp
src/mc/mc_config.hpp
src/mc/mc_hash.hpp
src/mc/mc_smx.hpp
src/mc/mc_unw.hpp
src/mc/remote/mc_protocol.h
src/mc/ObjectInformation.hpp
src/mc/Frame.hpp
src/mc/Variable.hpp

- Functions:
  * take_snapshot() and its variants
  * restore_snapshot() and its variants


*** Meeting with prof. Quinson
- TODO:
  * find which functionalities DMTCP can outsource from libsosp

*** How can DMTCP use libsosp?
**** write_memory_areas(int fd)
Currently this is the interface used by DMTCP to write a checkpoint to disk.\\
In the implementation we envision we could use a similar interface.
"sosp_take_snapshot(int fd)"
**** sosp_take_snapshot(int fd)
libsosp functionalities that "sosp_take_snapshot()" could use:
- libsosp allocates memory for the page store
- libsosp takes snapshot and stores them in the page store
- libsosp hashes the pages of the snapshot, to avoid page duplicates in the page store\\
In addition to the above features of libsosp "sosp_take_snapshot()" would:
- From the second checkpoint taken, check if the pages are already in the page store
- Write to disk only those pages that are not in the page store
- Change the format of the ckpt image on disk to take care of the pages that are now scattered
- We can also have a page store on disk, just like the one in memory
**** challenges (currently)
- Reduce libsosp dependency to SimGrid
- Identify the parts where DMTCP and SimGrid differ and use preprocessor directives

**** readmemoryareas(int fd) //for restart
Currently this is the interface used by DMTCP to read a checkpoint image from Disk.\\
We could use a similar interface to restore a snapshot, "sosp_restore_snapshot()".\\

**** sosp_restore_snapshot()
libsosp functionalities that "sosp_restore_snapshot()" could use:
- libsosp restores memory from a snapshot
In the case of DMTCP the snapshot will be read from Disk

*** simgrid vs dmtcp

**** take_snapshot() // mc_checkpoint.cpp
  * "mc_model_checker", absent in dmtcp
  * snapshot->enable_processes, #nt in dmtcp
  * snapshot_handle_ignore(), this is done differently in dmtcp
  * in dmtcp even the text segment is copied
  * get_current_fds, file descriptors are handled #ntly in dmtcp
  * get_memory_regions, can be implemented the same way
  * visited states, absent in dmtcp

***** mc_model_checker
All threads reside in the same process, even the ckpt thread, in the case of dmtcp.\\
On the other hand, in simgrid the model checker acts like the ckpt thread in a separated process.
***** snapshot_handle_ignore()
For SimGrid the ignored regions are read in some buffer. Whereas for DMTCP, they are literally ignored.
***** file descriptors
In dmtcp, there is a plugin for handling file descriptors. It's a wrapper plugin.

***** get_memory_regions()
They can both share this function.

**** restore_snapshot() // mc_checkpoint.cpp
 * restore_snapshot_regions()
 * snapshot_ignore_restore()
 

